CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommendations
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

UIKit is a new lightweight framework that has become a popular alternative to
Bootstrap. UIkit GUI will speed up design and development of websites
considerably and providing a rapid and streamlined workflow for adding features
like slideshows, accordions, tabs, grid stacks, animations all from a single
back-end page.

 * For a full description of the theme, visit the project page:
   https://www.drupal.org/project/uikit_gui

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/uikit_gui


REQUIREMENTS
------------

    There are no requirements to using UIkit GUI.


RECOMMENDATIONS
------------

The following projects are recommended and fully supported by UIkit GUI.

 *  UIkit
    https://www.drupal.org/project/uikit

    You will need to use the UIkit base theme in order for the UIkit GUI
    components to be styled correctly.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-modules
   for further information.


CONFIGURATION
-------------

The module settings can be found at admin/config/user-interface/uikit-gui.


MAINTAINERS
-----------

Current maintainers:
 * kingfisher64
   - https://www.drupal.org/u/kingfisher64
 * Richard C Buchanan, III (Richard Buchanan)
   - https://www.drupal.org/u/richard-buchanan
